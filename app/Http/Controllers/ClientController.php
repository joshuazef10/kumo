<?php

namespace App\Http\Controllers;

use App\Models\ClientsTest;
use Illuminate\Http\Request;

class ClientController extends Controller
{
    public function store(Request $request)
    {
        $request->validate([
            'company' => 'required|string',
            'name' => 'required|string',
            'phone' => 'required|numeric',
            'mobile' => 'required|numeric',
            'email' => 'required|email|unique:clients_test,email',
            'credit_limit' => 'required',
        ]);

        ClientsTest::create([
            'company' => $request->company,
            'name' => $request->name,
            'phone' => $request->phone,
            'mobile' => $request->mobile,
            'email' => $request->email,
            'credit_limit' => $request->credit_limit,
        ]);

        return;
    }

    public function update(Request $request, ClientsTest $clientsTest)
    {
        $request->validate([
            'company' => 'required|string',
            'name' => 'required|string',
            'phone' => 'required|numeric',
            'mobile' => 'required|numeric',
            'email' => 'required|email|unique:clients_test,email,'.$clientsTest->id,
            'credit_limit' => 'required',
        ]);

        $clientsTest->company = $request->company;
        $clientsTest->name = $request->name;
        $clientsTest->phone = $request->phone;
        $clientsTest->mobile = $request->mobile;
        $clientsTest->email = $request->email;
        $clientsTest->credit_limit = $request->credit_limit;
        $clientsTest->save();

        return;
    }

    public function get(ClientsTest $clientsTest)
    {
        return response()->json($clientsTest);
    }

    public function delete(ClientsTest $clientsTest)
    {
        $clientsTest->delete();
        
        return;
    }
}
