<?php

namespace App\Http\Controllers;

use App\Models\ClientsTest;
use App\Models\LogCdr;
use Carbon\Carbon;
use GuzzleHttp\Client;
use Illuminate\Http\Request;

class ModelController extends Controller
{
    public function modelOne()
    {
        return view('modelOne.index');
    }

    public function table()
    {
        $clients = ClientsTest::orderBy('id','desc')->paginate(2);

        return view('modelOne.partials._table', compact('clients'));
    }

    public function modelTwo()
    {
        return view('modelTwo.index');
    }

    public function getData(Request $request)
    {

        $start = ( empty($request->start) )? '' : Carbon::parse($request->start)->format('Y-m-d');
        $end   = ( empty($request->end) )? '' : Carbon::parse($request->end)->subDays(10)->format('Y-m-d');

        session(['startDay' => $start]);
        session(['endDay' => $end]);

        $data = LogCdr::where('started', $start)
                      ->orWhere('ended', $end)
                      ->paginate(2);

        return view('modelTwo.partials._table', compact('data'));
    }

    public function modelThree()
    {
        $client = new Client();

        $url = config('variables.url');
        $api_id  = config('variables.api_id');
        $api_key = config('variables.api_key');

        $response = $client->request('GET', $url.'?api_id='.$api_id.'&api_key='.$api_key.'&call=getClients');

        $response = json_decode(config('variables.api_response'), true);
        $clients = collect($response['clients']);

        // dd(collect(json_decode($response->getBody(),true)));
        
        return view('modelThree.index', compact('clients'));
    }
}
