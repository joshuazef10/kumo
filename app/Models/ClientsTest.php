<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ClientsTest extends Model
{
    protected $table = 'clients_test';
    protected $fillable = [
        'company',
        'name',
        'phone',
        'mobile',
        'email',
        'suspended',
        'credit_limit',
    ];
}
