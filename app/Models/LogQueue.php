<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LogQueue extends Model
{
    protected $table = 'log_queue';
}
