<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LogCdr extends Model
{
    protected $table = 'log_cdr';

    public function getMinutesAttribute()
    {
        return ($this->billsec)/60;
    }

    public function agent()
    {
        return $this->belongsTo(LogQueue::class,'agent_id','id');
    }
}
