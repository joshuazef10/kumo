<?php
return [
    'api_id' => env('API_ID'),
    'api_key' => env('API_KEY'),
    'url'    => 'https://data-center.mx/Intra/api.php',
    'api_response' => '{
        "success": true,
        "clients": [
           {
              "id": "8",
              "firstname": "Jack",
              "lastname": "Black",
              "datecreated": "2011-11-12",
              "email": "jackblack@domain.us",
              "companyname": "",
              "services": 2
           },
           {
              "id": "6",
              "firstname": "Mike",
              "lastname": "Blue",
              "datecreated": "2011-11-12",
              "email": "admin@eml.com",
              "companyname": "",
              "services": 3
           },
           {
              "id": "4",
              "firstname": "Jonah",
              "lastname": "Hamon",
              "datecreated": "2011-10-28",
              "email": "john@doc.com",
              "companyname": "",
              "services": 8
           },
           {
              "id": "3",
              "firstname": "Administrator",
              "lastname": "Hostbill",
              "datecreated": "2011-10-28",
              "email": "admin@hostbillapp.com",
              "companyname": "",
              "services": 1
           },
           {
              "id": "2",
              "firstname": "Random",
              "lastname": "Customer",
              "datecreated": "2011-10-05",
              "email": "localclient@mail.com",
              "companyname": "companyname",
              "services": 13
           },
           {
              "id": "1",
              "firstname": "John",
              "lastname": "Doe",
              "datecreated": "2011-09-24",
              "email": "jondoe@email.com",
              "companyname": "",
              "services": 26
           }
        ],
        "call": "getClients",
        "server_time": 1323444040
     }',
];
