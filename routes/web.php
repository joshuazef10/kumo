<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/model-one','ModelController@modelOne')->name('model.one.index');
Route::get('/table','ModelController@table')->name('model.one.table');

Route::get('/get-client/{clients_test}','ClientController@get')->name('model.one.get.client');
Route::post('/store-client/{page?}','ClientController@store')->name('model.one.store.client');
Route::post('/edit-client/{clients_test}','ClientController@update')->name('model.one.update.client');
Route::post('/delete-client/{clients_test}','ClientController@delete')->name('model.one.update.client');

Route::get('/model-two','ModelController@modelTwo')->name('model.two.index');
Route::get('/report','ModelController@getData')->name('model.two.report');

Route::get('/model-three','ModelController@modelThree')->name('model.three.index');

