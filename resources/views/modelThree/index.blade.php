@extends('layouts.master')

@section('content')
    <div class="container" controller="model-three">
        @include('modelThree.partials._table')
    </div>
@stop
