<td class="center">
    <a class="tool" data-original-title="{{$client['id']}}" data-toggle="tooltip" data-placement="top" data-html="true" title="@include('modelThree.partials._tooltip',[
        'id' => $client['id'],
        'firstname'=> $client['firstname'],
        'lastname'=> $client['lastname'],
        'email' => $client['email'],
        ])" href="#">{{ $client[$key] }}</a>
</td>
