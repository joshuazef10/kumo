<link rel="stylesheet" href="{{ asset('css/app.css') }}">
<table class="table table-bordered table-dark">
    <thead>
        <tr>
            <th scope="col">ID</th>
            <th scope="col">First Name</th>
            <th scope="col">Last Name</th>
            <th scope="col">Date Created</th>
            <th scope="col">Email</th>
            <th scope="col">Company Name</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($clients as $client)
        <tr>
            @include('modelThree.partials._td',['key' => 'id'])
            @include('modelThree.partials._td',['key' => 'firstname'])
            @include('modelThree.partials._td',['key' => 'lastname'])
            @include('modelThree.partials._td',['key' => 'datecreated'])
            @include('modelThree.partials._td',['key' => 'email'])
            @include('modelThree.partials._td',['key' => 'companyname'])
        </tr>
        @endforeach
    </tbody>
</table>
