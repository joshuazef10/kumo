@extends('layouts.master')

@section('content')
<div class="container" controller="model-one">
    <div class="row">
        <div class="col-12 pt-2">
            <input type="text" name="q" placeholder="Search">
            <div class="d-flex align-items-end flex-column">
                <button type="button" class="btn btn-success" data-toggle="modal" data-target=".bd-example-modal-lg">
                    Add Client
                </button>
            </div>
            <br>
        </div>
        <br><br>
        <div id="table-client">
        </div>
    </div>
</div>
@include('modelOne.partials._modal_add')
@include('modelOne.partials._modal_edit')
@stop
