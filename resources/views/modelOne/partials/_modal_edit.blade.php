<div class="modal fade modal-edit-client" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Edit Client  <small style="font-size: 12px;">* Fields required</small></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="alert alert-danger" role="alert" style="display:none;">
                        <strong id="errors-edit"></strong>
                    </div>
                    <form id="editClient" method="POST">
                        @csrf
                        <input type="hidden" name="clientId" class="clientId">
                        <div class="form-group">
                            <label for="company">Company*</label>
                            <input type="text" class="form-control" name="company" >
                        </div>
                        <div class="form-group">
                            <label for="name">Name client*</label>
                            <input type="text" class="form-control" name="name">
                        </div>
                        <div class="form-group">
                            <label for="phone">Phone*</label>
                            <input type="text" class="form-control" name="phone">
                            <small>only digits</small>
                        </div>
                        <div class="form-group">
                            <label for="mobile">Mobile*</label>
                            <input type="text" class="form-control" name="mobile">
                            <small>only digits</small>
                        </div>
                        <div class="form-group">
                            <label for="email">E-mail*</label>
                            <input type="text" class="form-control" name="email">
                        </div>
                        <div class="form-group">
                            <label for="credit_limit">Credit Limit*</label>
                            <input type="text" class="form-control" name="credit_limit">
                        </div>
                        <div class="text-right">
                            <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                            <button type="submit" class="btn btn-success">Save</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
