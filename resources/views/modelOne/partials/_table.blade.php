<link rel="stylesheet" href="{{ asset('css/app.css') }}">
<table class="table table-bordered table-dark">
        <thead>
            <tr>
                <th scope="col">ID</th>
                <th scope="col">Company</th>
                <th scope="col">Client</th>
                <th scope="col">Phone</th>
                <th scope="col">Mobile</th>
                <th scope="col">Email</th>
                <th scope="col center">Suspended</th>
                <th scope="col">Credit Limit</th>
                <th scope="col"><span>+</span></th>
            </tr>
        </thead>
        <tbody>
            @foreach ($clients as $client)
            <tr>
                <th scope="row">{{$client->id}}</th>
                <td>{{$client->company}}</td>
                <td>{{$client->name}}</td>
                <td>{{$client->phone}}</td>
                <td>{{$client->mobile}}</td>
                <td>{{$client->email}}</td>
                <td>{{$client->suspended}}</td>
                <td>{{$client->credit_limit}}</td>
                <td>
                    <button type="button" class="btn btn-info btn-sm btn-block" data-target=".modal-edit-client" data-toggle="modal" data-client-id="{{$client->id}}" style="display:block;">Edit</button>
                    <form class="form-inline" name="deleteClient">
                        @csrf
                        <input type="hidden" name="client_id" value="{{$client->id}}">
                        <button type="submit" class="mt-2 btn btn-danger btn-sm btn-block ">Delete</button>
                    </form>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
    <div class="col-12">
        {{ $clients->links() }}
    </div>
