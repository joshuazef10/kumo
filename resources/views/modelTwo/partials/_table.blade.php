<link rel="stylesheet" href="{{ asset('css/app.css') }}">
<table class="table table-bordered table-dark">
    <thead>
        <tr>
            <th scope="col">Client</th>
            <th scope="col">Date Start</th>
            <th scope="col">Date End</th>
            <th scope="col">Minutes</th>
            <th scope="col">Agent</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($data as $item)
        <tr>
            <td>{{$item->client}}</td>
            <td>{{$item->started}}</td>
            <td>{{$item->ended}}</td>
            <td>{{$item->billsec}}</td>
            <td>{{$item->agent->name}}</td>
        </tr>
        @endforeach
    </tbody>
</table>
<div class="col-12">
    {{ $data->appends(['start' => session('startDay'), 'end' => session('endDay')])->links() }}
</div>
