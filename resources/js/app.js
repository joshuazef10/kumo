require('./bootstrap');


// Dispatch
import dispatch from './controllers'

// Initializers
const docReady = () => {
  new dispatch().init()
}

const winLoad = () => {
  // ...
}

const docReadyWinload = () => {
  // ...
}

// ---
// [IMPORTANT] Do not change or add anything!
document.addEventListener('DOMContentLoaded', () => {
  docReady()
  docReadyWinload()
}, false)
window.addEventListener('load', () => {
  winLoad()
  docReadyWinload()
}, false )
// ---
