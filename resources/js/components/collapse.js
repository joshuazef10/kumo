class collapse {
  toggleCollapse($collapse, $collapseGroup) { // jquery
    const $collapseContent = $($collapse.nextElementSibling)

    if (!$collapseGroup.hasAttribute('collapse-multiple')) {
      $($collapseGroup).find('[collapse-content]').not($collapseContent).stop().slideUp(150, function() {
        $($collapseGroup).find('[collapse-click]').removeAttr('collapse-active')
      })
    }

    $collapseContent.stop().slideToggle(150, function() {
      $collapse.removeAttribute('collapse-active')

      if ($collapseContent.is(':visible')) {
        $collapse.setAttribute('collapse-active', true)
      }
    })
  }
  init() {
    document.querySelectorAll('[collapse-group]').forEach(($collapseGroup, i) => {
      $collapseGroup.querySelectorAll('[collapse-click]').forEach(($collapse, i) => {
        $collapse.addEventListener('click', (e) => {
          e.preventDefault()
          this.toggleCollapse($collapse, $collapseGroup)
        })
      })
    })
  }
}

export default collapse
