const changeTab = ($tab) => {
  const target = $tab.getAttribute('tabs-filters-show')

  document.querySelectorAll('[tabs-filters-content]').forEach(($tabsContent) => {
    $tabsContent.style.display = 'none'
  })

  document.querySelectorAll('[tabs-filters-show]').forEach(($tabsShow) => {
    $tabsShow.classList.remove('--active')
  })

  $tab.classList.add('--active')
  document.querySelector('[tabs-filters-content="' + target + '"]').style.display = 'block'
}

class tabsFilters {
  init() {
    document.querySelectorAll('[tabs-filters]').forEach(($tabs) => {
      $tabs.querySelectorAll('[tabs-filters-show]').forEach(($tab) => {
        $tab.addEventListener('click', (e) => {
          e.preventDefault()

          changeTab($tab)
        })
      })
    })
  }
}

export default tabsFilters
