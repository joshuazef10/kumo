require('jquery-confirm')

const btnClose = '<span class="x:fs-18 fas fa-times"></span>'

const defaultSettings = {
  animation        : 'scale',
  backgroundDismiss: true,
  closeAnimation   : 'scale',
  smoothContent    : false,
  theme            : 'modern',
  typeAnimated     : false,
  useBootstrap     : false,
}

const defaultBtns = {
  buttons: {
    ok: {
      btnClass: 'btn--blue-white btn--large x:mrg-top-10',
      text: '<div>Aceptar <img src="/dist/img/default/mano.png" style="width: 20px;" /></div>'
    },
    close: {
      btnClass: 'btn--alert-close',
      text: '<span class="x:fs-18 fas fa-times"></span>'
    }
  }
}

const confirmForm = (e, $target) => {
  const title      = $target.getAttribute('alert-title')
  const content    = $target.getAttribute('alert-content') || ''
  const btnOk      = $target.getAttribute('alert-ok') || "Continuar"
  const btnCancel  = $target.getAttribute('alert-cancel') || "Cancelar"

  let confirmOptions = {
    title: title,
    content: content,
    type   : 'neutral',
    buttons: {
      cancel: {
        text: '<div>' + btnCancel + '</div>',
        btnClass: 'btn btn--grey btn--large x:mrg-bt-5',
      },
      confirm: {
        text: '<div>' + btnOk + '</div>',
        btnClass: 'btn btn--blue btn--large x:mrg-bt-5',
        action: function(target){
          $target.closest('form').submit()
        }
      },
      close: {
        text: btnClose,
        btnClass: 'btn--alert-close'
      }
    }
  }

  confirmOptions = $.extend(defaultSettings, confirmOptions)

  $.confirm(confirmOptions)
}

const confirmLink = (e, $target) => {
  const title      = $target.getAttribute('alert-title')
  const content    = $target.getAttribute('alert-content') || ''
  const btnOk      = $target.getAttribute('alert-ok') || "Continuar"
  const btnCancel  = $target.getAttribute('alert-cancel') || "Cancelar"
  const method     = $target.getAttribute('alert-method') || "get"
  const targetLink = $target.getAttribute('href')

  let confirmOptions = {
    title: title,
    content: content || "",
    type   : 'neutral',
    buttons: {
      confirm: {
        text: btnOk,
        btnClass: 'btn btn--blue-o btn--large',
        action: function(target){
          if (method === 'get') {
            window.location.href = targetLink

            return false
          }

          const csrfToken = document.querySelector('meta[name="csrf-token"]').getAttribute('content')
          let   formGhost

          formGhost  = '<form action="' + targetLink + '" method="post">'
          formGhost +=  '<input type="hidden" name="_token" value="' + csrfToken + '">'
          formGhost +=  '<input type="hidden" name="_method" value="' + method + '">'
          formGhost += '</form>'

          $(formGhost).appendTo('body').submit()
        }
      },
      cancel: {
        text: btnCancel,
        btnClass: 'btn btn--link btn--large',
      },
      close: {
        text: btnClose,
        btnClass: 'btn--alert-close'
      }
    }
  }

  confirmOptions = $.extend(defaultSettings, confirmOptions)

  $.confirm(confirmOptions)
}

const message = (e, $target) => {
  let msg = {
    title: $target.getAttribute('alert-title'),
    content: $target.getAttribute('alert-content'),
    type   : 'neutral',
  }

  msg = $.extend(defaultSettings, defaultBtns, msg)

  $.confirm(msg)
}

const launch = () => {
  document.querySelectorAll('[alert="confirm"]').forEach(($item) => {
    $item.addEventListener('click', (e) => {
      e.preventDefault()

      confirm(e, e.target)
    })
  })

  document.querySelectorAll('[alert="message"]').forEach(($item) => {
    $item.addEventListener('click', (e) => {
      e.preventDefault()

      message(e, e.target)
    })
  })
}

class defaultAlerts {
  errors() {
    let content = app.errors

    if (content.constructor === Array) {
      content = '<ul><li>' + app.errors.join('</li><li>') + '</li></ul>'
    }

    let errorOptions = {
      defaultBtns,
      content: content,
      icon   : 'far fa-times-circle x:fs-40',
      title  : 'Hay un problema',
      type   : 'red',
    }

    errorOptions = $.extend(defaultSettings, errorOptions, defaultBtns)

    if (typeof app.errors != 'undefined' && app.errors.length > 0) {
      $.confirm(errorOptions)
    }
  }
  success() {
    const content = '<p>' + app.success + '</p>'

    let successOptions = {
      defaultBtns,
      content: content,
      icon   : 'far fa-check-circle x:fs-50',
      title  : 'Completado',
      type   : 'green',
    }

    successOptions = $.extend(defaultSettings, successOptions, defaultBtns)

    if (typeof app.success != 'undefined' && app.success.length > 0) {
      $.confirm(successOptions)
    }
  }
  launch() {
    if (app.success.length > 0)
      this.success()

    if (app.errors.length > 0)
      this.errors()

    document.querySelectorAll('[alert]').forEach(($item) => {
      $item.addEventListener('click', (e) => {
        e.preventDefault()
        const alertType = $item.getAttribute('alert')

        if (alertType === 'confirm-form')
          confirmForm(e, e.target)

        if (alertType === 'confirm-link')
          confirmLink(e, e.target)

        if (alertType === 'message')
          message(e, e.target.closest('a, button'))
      })
    })
  }
  init() {
    this.launch()
  }
}

export default defaultAlerts
