const scrollToSection = ($item) => {
  const target = $item.getAttribute('href')
  const $target = document.querySelector(target)

  console.log()

  // jQuery
  $('html, body').stop().animate({
    scrollTop: $($target).offset().top - document.querySelector('.main-menu').getBoundingClientRect().height + 5
  })
}

class anchorAnimated {
  init() {
    document.querySelectorAll('[anchor-animated]').forEach(($item) => {
      $item.addEventListener('click', (e) => {
        e.preventDefault()

        scrollToSection($item)
      })
    })
  }
}

export default anchorAnimated
