const toggleSideMenu = ($mainMenu) => {
  $mainMenu.classList.toggle('open')
}

const followme = ($mainMenu) => {
  const scrolled = window.pageYOffset
  const mainMenuHeight = $mainMenu.getBoundingClientRect().height - 80

  $mainMenu.classList.remove('--followme')

  if (scrolled >= mainMenuHeight)
    $mainMenu.classList.add('--followme')
}

class openSideMenu {
  constructor() {
    this.DOM = {
      'mainMenu': document.querySelector('.main-menu'),
      'openSideMenu': document.querySelector('.open-side-menu'),
      'sideMenu': document.querySelector('.side-menu')
    }
  }
  init() {
    if(this.DOM.openSideMenu != null && this.DOM.openSideMenu != '' && this.DOM.openSideMenu != 'undefined' ){
      this.DOM.openSideMenu.addEventListener('click', (e) => {
        e.preventDefault()
        e.stopPropagation()

        toggleSideMenu(this.DOM.mainMenu)
      })

      this.DOM.sideMenu.addEventListener('click', (e) => {
        e.stopPropagation()
      })

      document.addEventListener('click', () => {
        if (this.DOM.mainMenu.classList.contains('open'))
          toggleSideMenu(this.DOM.mainMenu)
      })

      followme(this.DOM.mainMenu)

      window.addEventListener('scroll', () => {
        followme(this.DOM.mainMenu)
      })
    }
  }
}

export default openSideMenu
