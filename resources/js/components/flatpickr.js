import flatpickr from "flatpickr"
import { Spanish } from "flatpickr/dist/l10n/es.js"

class flatpickrInput {
  init() {
    flatpickr("input[type='flatpickr']", {
      animate: false,
      dateFormat: 'Y-m-d',
      altFormat: 'd/m/Y',
      altInput: true,
      locale: {
        ...Spanish,
        firstDayOfWeek: 0
      }
    });
  }
}

export default flatpickrInput
