const init = () => {
    start();

    function start(){
        $(document).ready(function(){
            $('.tool').tooltip({
                trigger:'hover'
            });

            $('.tool').on('click',function(e){
                e.preventDefault();
            });
        });
    }
}
  export default init
