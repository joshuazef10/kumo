const init = () => {

    listClients();
    lisentEdit();
    lisentDelete()
    lisentPaginate();

    $('#addClient').on('submit', function(e){
        e.preventDefault();
        var data = $(this).serialize();
        $.ajax({
            type: "POST",
            url: '/store-client',
            data: $('#addClient').serialize(),
            success: function (data) {
                $('.bd-example-modal-lg').modal('hide');
                listClients();
            },
            error:function(error){
                var msg = '';
                $.each( error.responseJSON.errors, function( key, value ) {
                    msg +=  value + '<br>';
                });

                $('#errors').html(msg);
                $('.alert-danger').fadeIn();
            },
        });
        $(this).trigger('reset');
    });

    $('#editClient').on('submit', function(e){
        e.preventDefault();
        var data = $(this).serialize(),
            clientId = $('.clientId').val();

        $.ajax({
            type: "POST",
            url: '/edit-client/'+clientId,
            data: $('#editClient').serialize(),
            success: function (data) {
                $('.modal-edit-client').modal('hide');
                listClients();
                $('#editClient').trigger('reset');
            },
            error:function(error){
                var msg = '';
                $.each( error.responseJSON.errors, function( key, value ) {
                    msg +=  value + '<br>';
                });

                $('#errors-edit').html(msg);
                $('.alert-danger').fadeIn();
            },
        });
    });

    function listClients(){
        $.ajax({
            type: "GET",
            url: '/table',
            success: function (data) {
                $('#table-client').empty().html(data)
            }
        })
    }

    function lisentPaginate(){
        $(document).on("click", ".pagination li a", function(e){
            e.preventDefault();
            var url = $(this).attr('href');
            $.ajax({
                type: "GET",
                url: url,
                success: function (data) {
                    $('#table-client').empty().html(data)
                }
            });
        });
    }

    function lisentEdit(){
        $(document).on("click", ".btn-info", function(e){
            e.preventDefault();
            var clientId = $(this)[0].dataset.clientId;
            $.ajax({
                type: "GET",
                url: '/get-client/'+ clientId,
                success: function (data) {
                    var form = document.getElementById("editClient");
                    form.elements['clientId'].value = data.id;
                    form.elements['company'].value = data.company;
                    form.elements['name'].value = data.name;
                    form.elements['phone'].value = data.phone;
                    form.elements['mobile'].value = data.mobile;
                    form.elements['email'].value = data.email;
                    form.elements['credit_limit'].value = data.credit_limit;

                }
            })
        });
    }

    function lisentDelete(){
        $(document).on('submit', 'form[name="deleteClient"]', function(e){
            e.preventDefault();

            var form = $(this),
                clientId = form[0].elements['client_id'].value,
                _token = form[0].elements['_token'].value;

            $.ajax({
                type: "POST",
                url: '/delete-client/'+ clientId,
                data: {'_token':_token},
                success: function (data) {
                   listClients();
                }
            })
        });
    }

    }

  export default init
