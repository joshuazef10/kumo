const init = () => {
    listReport();
    mydatePicker();
    lisentPaginate();


    function mydatePicker(){
        var date = new Date();
            date.setDate(date.getDate());
            
        let start = '',
            end = '';

        $('#startDate').datepicker({
            language: 'es',
            endDate: date,
        })
        .on('changeDate', function(e) {
            start = e.format(0,"yyyy-mm-dd");
            var url     = '/report?start='+start+'&end='+end;
            $.ajax({
                type: "GET",
                url: url,
                success: function (data) {
                    $('#table-report').empty().html(data)
                }
            });
            $(this).datepicker('hide');
        });

        $('#endDate').datepicker({
            language: 'es',
            endDate: date,
        })
        .on('changeDate', function(e) {
            end = e.format(0,"yyyy-mm-dd");
            var url     = '/report?start='+start+'&end='+end;
                $.ajax({
                    type: "GET",
                    url: url,
                    success: function (data) {
                        $('#table-report').empty().html(data)
                    }
                });
                $(this).datepicker('hide');
        });

    }

    function lisentPaginate(){
        $(document).on("click", ".pagination li a", function(e){
            e.preventDefault();
            var url = $(this).attr('href');
            console.log(url);
            $.ajax({
                type: "GET",
                url: url,
                success: function (data) {
                    $('#table-report').empty().html(data)
                }
            });
        });
    }

    function listReport(){
        $.ajax({
            type: "GET",
            url: '/report',
            success: function (data) {
                $('#table-report').empty().html(data)
            }
        })
    }
}
  export default init
