// import * as msg from '../helpers/console'
import controllers from './controllers'

class dispatch {
  controllerAvaliable(controller) {
    if(controller === '0' && controller !== '') {
      return false
    }

    if(controller === '') {
    //   msg.warning('Dispatch', 'Empty controller')
      return false
    }

    if(typeof controllers[controller] === 'undefined') {
    //   msg.error('Dispatch', '"' + controller + '"' + ' is undefined')
      return false
    }

    return true
  }
  init() {
    const controller = document.querySelector('[controller]').getAttribute("controller")

    if (this.controllerAvaliable(controller)) {
      controllers[controller].default()
    }
  }
}

export default dispatch
