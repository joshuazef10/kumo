const controllers = {
  'model-one': require('./views/model-one'),
  'model-two': require('./views/model-two'),
  'model-three': require('./views/model-three'),
}

export default controllers
