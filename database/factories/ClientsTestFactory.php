<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\ClientsTest;
use Faker\Generator as Faker;

$factory->define(ClientsTest::class, function (Faker $faker) {
    $suspended = $faker->randomElement(['yes', 'no']);
    return [
        // ClientsFactory
        'company' => $faker->company,
        'name' => $faker->name,
        'phone' => $faker->phoneNumber,
        'mobile' => $faker->phoneNumber,
        'email' => $faker->unique()->safeEmail,
        'suspended' => $suspended,
        'credit_limit' => $faker->randomFloat(3, 0, 1000),
    ];
});
