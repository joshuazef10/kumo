<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\LogQueue;
use Faker\Generator as Faker;

$factory->define(LogQueue::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
    ];
});
