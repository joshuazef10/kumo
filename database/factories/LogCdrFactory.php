<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\LogCdr;
use App\Models\LogQueue;
use Faker\Generator as Faker;

$factory->define(LogCdr::class, function (Faker $faker) {
    return [
        'agent_id' =>  function () {
            return LogQueue::first()->id;
        },
        'client'  => $faker->name,
        'started' => $faker->date(),
        'ended'   => $faker->date(),
        'billsec' => $faker->randomNumber(),
    ];
});
