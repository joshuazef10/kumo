<?php

use App\Models\ClientsTest;
use Illuminate\Database\Seeder;

class ClientsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(ClientsTest::class, 5)->create();
    }
}
