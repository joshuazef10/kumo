<?php

use App\Models\LogQueue;
use Illuminate\Database\Seeder;

class LogQueueTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(LogQueue::class, 5)->create();
    }
}
