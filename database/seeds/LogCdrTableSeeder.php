<?php

use App\Models\LogCdr;
use App\Models\LogQueue;
use Illuminate\Database\Seeder;

class LogCdrTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(LogCdr::class, 1000)->create([
            'agent_id' => function (){
                return LogQueue::inRandomOrder()->first()->id;
            },
        ]);
    }
}
