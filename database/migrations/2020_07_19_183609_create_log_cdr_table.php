<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLogCdrTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('log_cdr', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('agent_id')->unsigned()->index();
            $table->string('client');
            $table->date('started');
            $table->date('ended');
            $table->integer('billsec');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('agent_id')->references('id')->on('log_queue');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('log_cdr');
    }
}
