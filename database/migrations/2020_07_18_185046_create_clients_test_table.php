<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClientsTestTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clients_test', function (Blueprint $table) {
            $table->id();
            $table->string('company');
            $table->string('name');
            $table->string('phone');
            $table->string('mobile');
            $table->string('email');
            $table->string('suspended')->default('no');
            $table->float('credit_limit', 8, 2);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clients_test');
    }
}
